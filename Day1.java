package firsttestngpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

//Comment of javafile
public class Day1 {
	public String baseUrl = "http://www.demo.guru99.com/V4/";
	String driverPath = "D:\\Other\\Eclipse_Java\\geckodriver.exe";
	public WebDriver driver = new FirefoxDriver();

	@Test(priority = 0)
	public void Registration() {

		driver.navigate().to(baseUrl);

		WebElement login = driver.findElement(By.name("uid"));
		login.sendKeys("mngr165442");
		WebElement pass = driver.findElement(By.name("password"));
		pass.sendKeys("AzugydE");
		WebElement submit = driver.findElement(By.name("btnLogin"));
		submit.submit();
		String text = driver.findElement(By.xpath("/html/body/div[2]/h2")).getText();
		driver.close();
		Assert.assertEquals(text, "Guru99 Bank");
		System.out.println(text);
	}

}